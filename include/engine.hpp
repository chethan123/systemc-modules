#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <iostream>

SC_MODULE(Engine) {
  SC_CTOR(Engine) { std::cout << "Engine constructor called" << std::endl; }

  ~Engine() { std::cout << "Engine destructor called" << std::endl; }
};

#endif
