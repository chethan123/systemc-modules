#ifndef CAR_HPP
#define CAR_HPP

class Body;
class Engine;

SC_MODULE(Car) {

  Body* body;
  Engine* engine;

  Car(sc_module_name name);
  ~Car();
};

#endif
