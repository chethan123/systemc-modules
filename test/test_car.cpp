#include <systemc.h>
#include <iostream>
#include "car.hpp"

int sc_main(int argc, char* argv[]) {

  std::cout << "SystemC main started" << std::endl;

  Car* car = new Car("car");

  std::cout << "Car instance created" << std::endl;

  sc_start();

  std::cout << "Simulation ended" << std::endl;

  delete car;

  return 0;
}
